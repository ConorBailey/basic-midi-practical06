//
//  MidiMessageComponent.cpp
//  JuceBasicWindow
//
//  Created by Conor Bailey on 01/11/2016.
//
//

#include "MidiMessageComponent.hpp"

MidiMessageComponent::MidiMessageComponent()

{
    mesType.addItem("Note", 1);
    mesType.addItem("Control", 2);
    mesType.addItem("Program", 3);
    mesType.addItem("Pitch Bend", 4);
    
    
    addAndMakeVisible(mesType);
    mesType.addListener (this);
    
    
    //set slider sype and make visible
    for (int i = 0;  i < 3; i ++)
    {
        sliders[i].setSliderStyle(Slider::IncDecButtons);
        addAndMakeVisible(sliders[i]);
        
    }
    
    //    add text editor to app
    for (int i = 0; i < 4; i ++)
    {
        addAndMakeVisible(title[i]);
    }
    
    
}

MidiMessageComponent::~MidiMessageComponent()
{
    
}

void MidiMessageComponent::resized()
{
    
    float width = getWidth() / 4;
    
    
    
    mesType.setBounds(0, 30, width, 20);
    sliders[0].setBounds(width, 30, width, 20);
    sliders[1].setBounds(width * 2, 30, width, 20);
    sliders[2].setBounds(width * 3, 30, width, 20);
    
    title[0].setBounds(0, 10, width, 20);
    title[1].setBounds(width, 10, width, 20);
    title[2].setBounds(width * 2, 10, width, 20);
    title[3].setBounds(width * 3, 10, width, 20);
    
    title[0].setText("Message Type", dontSendNotification);
    
    
}

void MidiMessageComponent::comboBoxChanged (ComboBox *comboBoxThatHasChanged)
{
    if (mesType.getSelectedId() == 1)
    {
        
        title[3].setVisible(true);
        
        
        title[1].setText("Channel", dontSendNotification);
        title[2].setText("Note Num", dontSendNotification);
        title[3].setText("Velocity", dontSendNotification);
        
        sliders[2].setVisible(true);
        
        
        sliders[0].setRange(1, 16,1);
        sliders[1].setRange(0, 127,1);
        sliders[2].setRange(0, 127,1);
        
        outputMsg = MidiMessage::noteOn ((int)sliders[0].getValue(),
                                         (int)sliders[1].getValue(),
                                         (float)sliders[2].getValue());

    }
    
    else if (mesType.getSelectedId() == 2)
    {
        
        title[3].setVisible(true);
        
        
        title[1].setText("Channel", dontSendNotification);
        title[2].setText("Type", dontSendNotification);
        title[3].setText("Value", dontSendNotification);
        
        sliders[2].setVisible(true);
        
        
        sliders[0].setRange(1, 16,1);
        sliders[1].setRange(0, 127,1);
        sliders[2].setRange(0, 127,1);
        
        outputMsg = MidiMessage::controllerEvent((int)sliders[0].getValue(),
                                                 (int)sliders[1].getValue(),
                                                 (int)sliders[2].getValue());
    }
    
    else if (mesType.getSelectedId() == 3)
    {
        title[1].setText("Channel", dontSendNotification);
        title[2].setText("Program Number", dontSendNotification);
        title[3].setVisible(false);
        
        
        sliders[0].setRange(1, 16,1);
        sliders[1].setRange(0, 127,1);
        
        sliders[2].setVisible(false);
        
        outputMsg = MidiMessage::programChange((int)sliders[0].getValue(),
                                               (int)sliders[1].getValue());
        
    }
    
    else if (mesType.getSelectedId() == 4)
    {
        title[1].setText("Channel", dontSendNotification);
        title[2].setText("Value", dontSendNotification);
        title[3].setVisible(false);
        
        sliders[0].setRange(1, 16,1);
        sliders[1].setRange(0, 127,1);
        
        sliders[2].setVisible(false);
        
        outputMsg = MidiMessage::pitchWheel((int)sliders[0].getValue(),
                                            (int)sliders[1].getValue());
        

    
    }
    
}

void MidiMessageComponent::paint (Graphics& g)
{
    
}


MidiMessage MidiMessageComponent::getMidiMessage()
{
    return outputMsg;
}