//
//  MidiMessageComponent.hpp
//  JuceBasicWindow
//
//  Created by Conor Bailey on 01/11/2016.
//
//

#ifndef MidiMessageComponent_hpp
#define MidiMessageComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"


#include <stdio.h>

class MidiMessageComponent : public Component,
                            public ComboBox::Listener
{
public:
    MidiMessageComponent();
    ~MidiMessageComponent();
    
    void resized() override;

    void paint (Graphics& g) override;
    
    void comboBoxChanged (ComboBox *comboBoxThatHasChanged) override;
    
    MidiMessage getMidiMessage();
    
    
private:
    ComboBox mesType;
    Slider sliders[3];
    Label title[4];
    MidiMessage outputMsg;
};

#endif /* MidiMessageComponent_hpp */
