/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize(400, 500);
    
        
    addAndMakeVisible(sendMsg);
    sendMsg.setButtonText("Send");
    sendMsg.addListener(this);
    
    
    addAndMakeVisible(GUI);
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.setDefaultMidiOutput ("SimpleSynth virtual input");
    
    addAndMakeVisible(midiLabel);

    midiLabel.setText("test", dontSendNotification);

}

MainComponent::~MainComponent()
{

    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}





void MainComponent::buttonClicked(Button* button)
{
    
    DBG("Button");
    
    
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(GUI.getMidiMessage());

}

void  MainComponent::handleIncomingMidiMessage (MidiInput* source,
                                const MidiMessage& message)

{
    
}

void MainComponent::resized()
{

    GUI.setBounds(0, 30, 335, 50);
    sendMsg.setBounds(345, 30, 50, 20);

    
}

void MainComponent::paint(Graphics& g)
{

    g.setColour(Colours::azure);
}