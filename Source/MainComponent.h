/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "MidiMessageComponent.hpp"



//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component, MidiInputCallback,
                        public Button::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();
    
    void resized() override;
    
    void paint(Graphics& g) override;
 
    void handleIncomingMidiMessage (MidiInput* source,
                                    const MidiMessage& message) override;
    

    void buttonClicked(Button* button) override;
    
private:
    
    AudioDeviceManager audioDeviceManager;
    Label midiLabel;
//    MidiMessage outputMsg;
    TextButton sendMsg;
    MidiMessageComponent GUI;

    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
